%*******************************************************************************
%*********************************** First Chapter *****************************
%*******************************************************************************

\chapter{Introduction}  %Title of the First Chapter

\ifpdf
    \graphicspath{{Chapter1/Figs/Raster/}{Chapter1/Figs/PDF/}{Chapter1/Figs/}}
\else
    \graphicspath{{Chapter1/Figs/Vector/}{Chapter1/Figs/}}
\fi


\section{Motivation} %Section - 1.1 
\label{section1.1}

Variations in process parameters affect the digital integrated circuit (IC) and they have become a critical issue 
faced by the IC designers. Such variations are results of not only the fluctuations during fabrication (length, 
width, oxide thickness etc.) but also some aging-related variations (NBTI, PBTI, HCI etc.). 
The consequence is that they make the integrated circuits behave non-deterministically in terms of power consumption, stability and maximum speed degradation. 
This is especially critical for SRAM arrays as they are usually implemented with small transistors to increase density. 
As the device geometries continue to shrink and fabrication technology scales beyond 32 nm process variations 
are expected to be an even greater obstacle to keep up with the promise of Moore's law. \cite{survey} \cite{jaksic}
~\\ \\
Meanwhile, most current superscalar microprocessors incorporate a RISC-like instruction set architecture (ISA). 
Such ISA implies that majority of the instruction operands reside in the register file. 
At the same time, high-end superscalar microprocessors that use out-of-order instruction processing have relied on
the use of bigger and bigger register files to expose and exploit instruction level parallelism (ILP). 
A register file is essentially an SRAM array which means it falls into one of the victims of the process variations.
~\\ \\
Another type of error, namely, \emph{soft error} is also constantly haunting the microprocessors. 
An integrated circuit is suffered from \emph{soft error} when one or more of the content bits are flipped. The nature of this type of error is temporal so it is also called \emph{transient error}.
Many sources contribute to \emph{soft errors}, including energetic radiation particles, capacitive coupling, electromagnet IC interference, and other sources of electrical noise.
The scaling of devices, operating voltages, and design margins for increasing performance and functionality 
raises concerns about the susceptibility of future-generation systems to \emph{soft errors}.
Historically, such \emph{soft errors} were mostly of concern when designing high-availability systems typically used in 
mission- or life-critical applications. However, because of the confluence of device and voltage scaling,
and increasing system complexity, experts forecast that transient errors will be a problem in future highly integrated hardware designs.

\section{Objectives and Contributions } %Section - 1.2
\label{section1.2}

The menacing process variations and soft errors will for sure affect the register file in the current 
and future technologies, rendering it unreliable; and consequently, bring down the performance of the entire processor. 
~\\ \\
One countermeasure is to use guard-banding by deploying sufficiently large SRAM cells in the register file in order
to minimize the impact of process variation. However, this will increase the register file area and, 
caused by the longer wires, impact negatively the access time. Albeit several techniques are available to increase
the robustness of any SRAM array (e.g. WL boosting, redundancy, ECC, etc) this thesis takes 
an alternate microarchitecture technique to try to remedy the performance degradation process variation 
and transient error introduce while incur as little overhead as possible.
~\\ \\
The register file cache (RFC) architecture was originally proposed in the paper \cite{antonio00} in hope to 
cope with the ever-increasing delay of the multi-ported multi-cycle register file. This thesis takes one step further by utilizing the results from \cite{antonio00} and explores the possible bottlenecks of the architecture 
as well as its potential capability as a variation- and soft-error- aware architecture.
~\\ \\
The following list is what has been done in this thesis:

\begin{itemize}
	\item The RFC architecture is implemented in a cycle-accurate x86-family architectural simulator with 
		the best prefetching/fetching policy combination reported in \cite{antonio00}.
	\item Two vital characteristic (number of entries of the RFC and the buses in between RFC and 
		the register file) of the RFC architecture and their impacts on the performance were analyzed in detail.
	\item The overall area and power of this RFC architecture is evaluated using McPAT \cite{mcpat}.
	\item A strong-weak combination, where the RFC architecture is prone to the variation and results in some 
		unusable cell blocks (decrease in size) while the register file itself is immune to the variation,
		was proposed to test the robustness against the process variations.
	\item Architecture Vulnerability Factor (AVF) \cite{avf} of the register file is computed using the architectural simulator. 
		Furthermore, a minimalistic soft-error handling mechanism were proposed and SEUs were randomly injected during the 
		simulation to evaluate its impact on the RFC architecture.
\end{itemize}

\section{Organization}  %Section - 1.3 
\label{section1.3}

The rest of the thesis is composed of the following chapters. Chapter 2 provides a brief background information on 
the process variation and soft error. Chapter 3 describes the register file cache architecture (RFC) proposed 
in \cite{antonio00} and the prefetching/fetching policies used in this thesis. 
Chapter 4 evaluates and analyzes the results as well as gives information on the experiment platform and some 
crucial microarchitectural parameters. Chapter 5 draws the conclusion.
