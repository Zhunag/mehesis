%*******************************************************************************
%****************************** Second Chapter *********************************
%*******************************************************************************
\chapter{Backgrounds}

\ifpdf
    \graphicspath{{Chapter2/Figs/Raster/}{Chapter2/Figs/PDF/}{Chapter2/Figs/}}
\else
    \graphicspath{{Chapter2/Figs/Vector/}{Chapter2/Figs/}}
\fi


\section{Process Variation}

\subsection{A Brief Introduction}
Successful design of digital integrated circuits (ICs) has often relied on complicated optimization among various
design specifications such as silicon area, speed, testability, design effort, and power dissipation. 
Such a traditional design approach inherently assumes that the electrical and physical properties of transistors 
are deterministic and hence, predictable over the device lifetime. However, with the silicon technology entering 
the sub 65-nm regime, transistors no longer act deterministically. Fluctuation in device dimensions due 
to manufacturing process (subwavelength lithography, chemical mechanical polishing, etc.) is a serious issue in nanometer technologies.
~\\ \\
Until approximately 0.35-$\mu$m technology node, process variation was inconsequential for the IC industry.
Circuits were mostly immune to minute variations because the variations were negligible compared to the nominal
device sizes. However, with the growing disparity between feature size and optical wavelengths of lithographic 
processes at scaled dimensions (below 90 nm), the issue of parameter variation is becoming severe. 

\subsection{Classification of Variations}

Variations can have two main components: \emph{interdie} and \emph{intradie}. 
Parametric variations between dies that come from different runs, lots, and wafers are categorized into
interdie variations whereas variation of transistor strengths within the same die are defined as intradie variations.
Fluctuations in length (L), width (W), oxide thickness ($T_{ox}$), flat-band conditions, etc., 
give rise to interdie process variations whereas line edge roughness (LER) or random dopant fluctuations 
(RDFs) cause intradie random variations in process parameters. Systems that are designed without consideration 
to process variations fail to meet the desired timing, power, stability, and quality specifications \cite{survey}.
~\\ \\
Negative bias temperature instability (NBTI), positive bias temperature instability (PBTI), hot carrier injection
(HCI), time-dependent dielectric breakdown (TDDB), and electromigration give rise to so-called \emph{aging variation}.
Temperature and voltage fluctuations fall under the category of environmental variations.

\subsubsection*{A. Spatial Variations}

The spatial process variations can be attributed to several sources, namely, subwavelength lithography, RDF,
LER, and so on. For the present technology regime (< 65 nm), the feature sizes of the devices are smaller than 
the wavelength of light, and therefore, printing the actual layout correctly is extremely difficult. 
This is further worsened by the processes that are carried out during fabrication. Some of the processes and 
corresponding sources of variations are the following:

\begin{itemize}
	\item wafer: topography, reflectivity;
	\item reticle: CD error, proximity effects, defects;
	\item stepper: lens heating, focus, dose, lens aberrations;
	\item etch: power, pressure, flow rate;
	\item resist: thickness, refractive index;
	\item develop: time, temperature, rinse;
	\item environment: humidity, pressure.
\end{itemize}
The above manufacturing processes result in fluctuations in L; W; $T_{OX}$ , $V_{TH}$ (interdie variations) 
as well as dopant atom concentration and line edge irregularities (intradie variations). 

\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.60]{spatial_variation.png}}
\caption[Types of process variations]{Types of spatial variations:(a) channel length variation, (b) RDF,
	(c) scaling trend of number of doping atoms in the channel, (d) LER. Source: \cite{survey}}
\label{spatial}
\end{figure}

\subsubsection*{B. Temporal Variations}
Due to scaled dimensions, the operating conditions of the ICs also change circuit behavior. 
Voltage and temperature fluctuation manifests as unpredictable circuit speed whereas persistent stress on 
the devices leads to systematic performance and reliability degradation. The devices degrade over a lifetime 
and may not retain their original specifications.
~\\ \\
One general degradation type is defect generation in the oxide bulk or at the $Si$–$SiO_2$ interface over time. 
The defects can increase leakage current through the gate dielectric, change transistor metrics such as 
the threshold voltage, or result in the device failure due to oxide breakdown.
\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.50]{temporal_variation.png}}
\caption[Types of temporal variations]{Types of temporal variations:(a) NBTI degradation process in PMOS, (b) Impact ionization due to HCI, (c) Percolation path due to TDDB. Source: \cite{survey}}
\label{temporal}
\end{figure}

The major classification of the temporal process variations are:
\begin{itemize}
	\item \emph{NBTI} \cite{nbti0} \cite{nbti1} \cite{nbti2} is one of the most important threats to PMOS 
		transistors in very large scale integration (VLSI) circuits. The electrical stress $V_{GS} < 0$ on the 
		transistor generates traps at the $Si$–$SiO_2$ interface (Figure \ref{temporal}). These defect sites 
		increase the threshold voltage, reduce channel mobility of the metal–oxide–semiconductor field-effect 
		transistors (MOSFETs), or induce parasitic capacitances and degrade the performance. Overall, significant 
		reduction in the performance metrics implies a shorter lifetime and poses a challenge for the IC 
		manufacturers. One special property of NBTI is the recovery mechanism, i.e., the interface traps can be 
		passivated partially when the stress is reduced.
	\item \emph{PBTI} \cite{pbti0} \cite{pbti1} is experienced by the n-type metal oxide–semiconductor (NMOS) 
		transistors similar to the NBTI in case of PMOS transistors. Until recently, NBTI effect for PMOS was 
		considered to be more severe than PBTI. However, due to introduction of high-k dielectric and metal gates 
		in sub-45-nm technologies, PBTI is becoming an equally important reliability concern.
	\item \emph{HCI} \cite{hci0} \cite{hci1} can create defects at the Si–SiO 2 interface near the drain edge as 
		well as in the oxide bulk.  The damage is due to carrier heating in the high electric field near the 
		drain side of the MOSFET, resulting in impact ionization and subsequent degradation 
		(Figure \ref{temporal}(b)).  Also, HCI occurs during the low-to-high transition of the gate of an NMOS, 
		and hence the degradation increases for high switching activity or higher frequency of operation. 
		Furthermore, the recovery in HCI is negligible, making it worse for alternating current (AC) stress 
		conditions.
	\item \emph{TDDB} \cite{tddb0} \cite{tddb1} also known as oxide breakdown, is a source of significant 
		reliability concern. When a sufficiently high electric field is applied across the dielectric
		gate of a transistor, continued degradation of the material results in the formation of conductive paths, 
		which may shorten the anode and the cathode (Figure \ref{temporal}(c)).
		The short circuit between the substrate and gate electrode results in oxide failure. 
		Once a conduction path forms, current flows through the path causing a sudden energy burst, which may 
		cause runaway thermal heating. The result may be a soft breakdown (if the device continues to function) 
		or hard breakdown (if the local melting of the oxide completely destroys the gate).
	\item \emph{Electromigration} \cite{electromigration} is the transport of material caused by the gradual 
		movement of the ions in a conductor due to the momentum transfer between conducting electrons and 
		diffusing metal atoms. The resulting thinning of the metal lines over time increases the resistance 
		of the wires and ultimately leads to path delay failures.
	\item \emph{Voltage and temperature fluctuations} Temporal variations like voltage and temperature fluctuations
		add to the circuit marginalities. The higher temperature decreases the $V_{TH}$ (good for speed) but 
		reduces the on current V reducing the overall speed of the design. Similarly, if a circuit that is 
		designed to operate at 1 V works at 950 mV due to voltage fluctuations, then the circuit speed would go
		below the specified rate. Since the voltage and the temperature depend on the operating conditions, 
		the speed becomes unpredictable.
\end{itemize}

\subsection{Impact on SRAM}
\subsubsection*{Impact by Process Variations}
The interdie parameter variations, coupled with intrinsic on-die variation in the process parameters 
(e.g., threshold voltage, channel length, channel width of transistors) result in the mismatches in the strength 
of different transistors in an SRAM cell. The device mismatches can result in the failure of SRAM cells \cite{survey}.
The parametric failures in SRAM cells are principally due to:
\begin{itemize}
	\item destructive read (i.e., flipping of the stored data in a cell while reading - known as read failure $R_F$ );
	\item unsuccessful write (inability to write to a cell - defined as write failure $W_F$ );
	\item an increase in the access time of the cell resulting in violation of the delay requirement - defined as
		access time failure $A_F$;
	\item destruction of the cell content in standby mode with the application of lower supply voltage 
		(primarily to reduce leakage in standby mode) - known as hold failure $H_F$.
\end{itemize}
Another important failure model for SRAMs (especially low-voltage SRAMs)s the pseudoread problem (illustrated in
Figure \ref{pseudoread}). When the word line is enabled to read address 0, the neighboring cells (belonging to 
different word) go through read disturbance. The three neighboring cells should be robust enough to sustain 
possible retention/weak write failures.

\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.50]{pseudoread.png}}
\caption[A pseudoread scenario]{Half select issue due to column multiplexing. The unselected words (highlighted in red) experience read disturbance when a particular word is being written. Source \cite{survey}}
\label{pseudoread}
\end{figure}

\subsubsection*{Impact from temporal degradation in memory}
As noted previously, parametric variations in memory arrays due to local mismatches among six transistors in a 
cell can lead to failures. As a result, NBTI degradation, which only affects the PMOS transistors. can have a
prominent effect in SRAM parametric failures. Therefore, an SRAM array with perfect SNM may experience failures
after few years due to temporal degradation of SNM.  Simulation results obtained for a constant ac stress at a
high temperature show that SNM of an SRAM cell can degrade by more than 9\% in three years \cite{survey}.
~\\ \\
Weaker PMOS transistors (due to NBTI) can manifest itself as increased read failures. On the other hand, it also
enables faster write operation, reducing statistical write failure probability. The results using Hspice are
shown in Figure \ref{degradation}(b) for three different conditions: 1) only RDF, 2) static NBTI shift with RDF, 
and 3) statistical NBTI variation combined with the RDF. Comparison between conditions 2) and 3) shows that the 
impact of NBTI variation can significantly increase the read failure probability of an SRAM cell. In contrast to 
the read operation, write function of an SRAM cell usually benefits from the NBTI degradation. Due to NBTI, 
PMOS pull-up transistor PL (Figure \ref{degradation}(c)) become weaker compared to the access transistor AXL, 
leading to a faster write operation \cite{survey}.

\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.50]{temporal_degradation.png}}
\caption[Temporal memory degradation]{(a) 6T SRAM bit-cell schematic. (b) SRAM read failure probability after a three-year NBTI stress. The failure probability increases due to combined RDF and NBTI. (c) Variation in write access time under temporal NBTI variation. The mean write time reduces but the spread increases. Source: \cite{survey}}
\label{degradation}
\end{figure}

\section{Soft Error}

Single Event Effects (SEEs) are induced by the interaction of an ionizing particle with electronic components. 
Ionizing particles can be primary (such as heavy ions in space environment or alpha particles produced by 
radioactive isotopes contained in the die or its packaging), or secondary (recoils) created by the nuclear 
interaction of a particle, like a neutron or a proton with silicon, oxygen or any other atom of the die. 
SEEs become possible when the collected fraction of the charge liberated by the ionizing particle is larger than 
the electric charge stored on a sensitive node.  A sensitive node is defined as a node in a circuit whose 
electrical potential can be modified by internal injection or collection of electrical charges. Usually, an
information bit is associated with a voltage level \cite{seu1}.
~\\ \\
Today, these effects are considered as an important challenge, which is strongly limiting the reliability and 
availability of electronic systems, and have motivated abundant research and development efforts in both the 
industry and academia. In memory devices, latches, and registers, single events are mainly called Single
Event Upsets (SEU). This corresponds to a flip of the cell state. When for one particle interaction many storage 
cells are upset, a Multi-Cell Upset (MCU) is obtained. If more than one bit in a word is upset by a single event, 
a (Multi-Bit Upset (MBU) is obtained \cite{seu1}.

\subsection{Single Event Upset (SEU)}

Technology scaling has made today’s designs much more susceptible to soft errors. The ever decreasing supply
voltages and nodal capacitances (required for constraining the power and making circuit transition faster) results 
in reduced critical charge (Qcrit) required to upset a node in digital circuits. The problem becomes more acute 
for aircraft and space electronics where high-energy neutrons at higher altitudes and heavy ions in space are 
more abundant. Because of the prevailing predictions that soft error rate is increasing exponentially, there is 
a growing trend in the community to adopt soft error rate as a design parameter along with speed, area and 
power requirements. Furthermore, for the high-end server market, networking switches and database applications, 
the reliability of the computing machine is even more important along with its performance \cite{seu0}.
~\\ \\
An SEU occurs when an ionizing particle strike modifies the electrical state of a storage cell, such that 
an error is produced when the cell is read. In an SRAM or a flip-flop, the state of the memory is reversed. 
In a DRAM, the charge stored can be slightly modified and interpreted as a wrong value by the read circuitry. 
In logic circuits, SEUs can also be obtained when a SET propagates through the combinational logic and is captured 
by a latch or a flip-flop \cite{seu1}.
~\\ \\
Soft errors due to cosmic rays are already making an impact in industry. In 2000, Sun Microsystems acknowledged 
cosmic ray strikes on unprotected cache memories as the cause of random crashes at major customer sites in its 
flagship Enterprise server line. Sun is documented to having lost a major customer to IBM from this episode. 
In 1996, Normand reported numerous incidents of cosmic ray strikes by studying the error logs of several large 
computer systems. The fear of cosmic ray strikes prompted Fujitsu to protect 80\% of its 200,000 latches in its 
recent SPARC processor with some form of error detection \cite{avf}.
\subsection{Soft Error Rate (SER)}
SER is the rate at which soft errors appear in a device for a given environment. When the particular environment 
is known or for a reference environment (NYC latitude at sea level), the SER rate can be given in FIT. 
In semiconductor memory, the sensitivity is often given in FIT/Mb or in FIT/device. One FIT (Failure In Time) 
is equal to a failure in $10^9$ h.
~\\ \\
The FIT value is either predicted by simulation or is the result of an experimental error measurement near 
a neutron generator representing as accurately as possible the real neutron energy spectrum. The cross-section 
can be used to calculate SER. If, with a total fluence of N $neutrons/cm^2$ , n errors are obtained in one device,
the cross-section is $\sigma$ = n/N. With $\phi$ the particle flux in the real environment, expressed in 
$n/cm^2/h$, the FIT value is: FIT value = $(n/N) * \phi * 10^9$
With a cross-section of $5 * 10^{14}$ $cm^2$ per bit, and $\phi = 13 n/cm^2/h$, 
the number of FIT/Mb is $5 * 10^{14} * 10^{16} * 13 * 10^9 =$ 650 FIT.
~\\ \\
Another metric vendors use to express an error budget at a reference altitude is called Mean Time Between Failures 
(MTBF). Errors are often further classified as undetected or detected. The former are typically referred to as 
\emph{silent data corruption} (SDC); the latter, \emph{detected unrecoverable errors} (DUE). Note that detected 
recoverable errors are not errors.
~\\ \\
A group of researchers from Intel published an study on measuring SER at sea-level (Weapon Neutron Research (WNR) 
test facility at Los Alamos, New Mexico) \cite{ser0}. They performed a comprehensive measurements and 
characterization of neutron-induced SER in advanced 90-nm logic CMOS technology and the neutron soft error rate 
dependency on voltage and area. Figure \ref{ser_scaling} is the wrap-up chart that shows the SRAM data as 
a function of the technology node. From the 0.25 $\mu$m to 90-nm node the data was measured. At the 65-nm node, 
the SER was obtained by extrapolation of the measured trends while assuming a supply voltage of 1.1V.
\begin{figure}[bhtp]
\centerline{
	\includegraphics[scale=0.50]{ser_sea.png}
	\includegraphics[scale=0.50]{ser_setup.png}
}
\caption[Beam energy spectrum compared to Sea level(left); Experimental setup for SER measurements(right)]{Beam energy spectrum compared to Sea level(left); Experimental setup for SER measurements(right). Source \cite{ser0}}
\label{sea_level}
\end{figure}

\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.50]{ser_scaling.png}}
\caption[Technology scaling of neutron SER in SRAM]{Technology scaling of neutron SER in SRAM. Source: \cite{ser0}}
\label{ser_scaling}
\end{figure}
~\\ \\
Current predictions show that typical raw FIT rate numbers for latches and SRAM cells vary between 0.001 -
0.01 FIT/bit at sea level (\cite{avf}). The FIT/bit is projected to remain in this range for the next several 
technology generations, unless microprocessors aggressively lower the supply voltage to reduce the overall power
dissipation of chip.
