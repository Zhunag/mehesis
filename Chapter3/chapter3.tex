\chapter{The Register File Cache (RFC) Architecture}

% **************************** Define Graphics Path **************************
\ifpdf
    \graphicspath{{Chapter3/Figs/Raster/}{Chapter3/Figs/PDF/}{Chapter3/Figs/}}
\else
    \graphicspath{{Chapter3/Figs/Vector/}{Chapter3/Figs/}}
\fi

\section{Impact of A Register File}

To quantify the performance hit caused by the reduction of available physical registers in the register file. 
The result is shown in Figure \ref{physreg_comparison} (Register file size refers to the size of both the integer 
physical register file and the FP physical register file). In both benchmark suites (INT and FP) a larger register 
file size leads to a gain in performance. Nevertheless, to some points (somewhere between 128 and 192 entries) the 
performance flattens. We can observe that the performance of a register file of 192 entries has no performance 
difference compared to a register file of 256 in both benchmark suites. This leads to an ideal size of a register 
file size in a modern out-of-order x86 microprocessor (see Table \ref{table:parameters} for more configuration 
details) in terms of performance, namely, somewhere around 192 and 256. A register file of such size with 8 read 
ports and 4 write ports can hardly achieve a one cycle access time and be 100\% process variation resistant. \\ \\ 

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{physreg_comparison_int.pdf}
		\includegraphics[scale=0.50]{physreg_comparison_fp.pdf}
	}
	\caption{Performance with various register file size}
	\label{physreg_comparison}
\end{figure}
The need of a large register file size of a modern out-of-order processor along with the requirement to implement 
a register file using large SRAM cells to minimize the impact of process variations leads to an overprovisioned 
register file. The register file provides the source operands and stores the results of most instructions. 
The register renaming mechanism of a dynamically scheduled processors rename logical registers to 
physical registers at run time such that each result produced by any instruction in-flight is allocated to a 
different physical register. Under this scenario, the register file access time could be critical. 
Compare to an ideal 1-cycle register file, a register file with 2-cycle access time 1) resolves branches one cycle 
later 2) requires an extra level of the bypass network which increases the complexity significantly.
~\\ \\
At the expense of a lower performance, the complexity of the bypass network of a 2-cycle register file can be 
reduced by only implementing one level of the bypass network. Figure \ref{2cycle} shows the performance comparison 
between an ideal 1-cycle register file and a 2-cycle register file in which only one level of bypass is implemented
. We chose to keep the bypass network from the last level (the second cycle of the 2-cycle writeback stage) 
because otherwise there would be an undesirable situation in which a value is available in one cycle via the 
bypass network and becomes inaccessible in the subsequent cycle then can be accessed via the register file in 
the next cycle. This would greatly increase the complexity of the issue logic. \\

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.70]{2cycle_int.pdf}}
	\centerline{\includegraphics[scale=0.70]{2cycle_fp.pdf}}
	\caption{Performance drop of a 2-cycle 1-bypass level register file compare to an ideal 1-cycle register file}
	\label{2cycle}
\end{figure}

In Figure \ref{2cycle}, it can be observed an average performance drop of around 13\% for a 2-cycle 1-bypass-level 
register file compared to an ideal 1-cycle register file (harmonic mean is used) in both SPEC06 INT and SPEC06 FP 
benchmark suites. This is quite a significant performance lose due to the larger register file access time and the 
lack of the full bypass network implementation.

\section{The RFC Architecture}

While originally proposed to cope with big (and slow) register files, we reevaluate the register file cache 
\cite{antonio00} as a mechanism to tolerate and hide the effect of process variations in the register file. The 
original proposal consists of a two-level cache-like register file architecture. Figure \ref{rfc_diagram} is an 
illustration of the aforementioned architecture. It is a heterogeneous design in the sense that there is a 
register file bank at each level and the two register file banks vary in terms of entries which in turn leads to 
a different access time. 

As shown in Figure \ref{rfc_diagram}, the levels in the register file cache are named as uppermost level and lowest
level. The register file bank at the uppermost level serves as the ``cache'' in this architecture. Hence its name, 
register file cache. It provides register values required by the instructions on-the-fly and stores register values
which are predicted to be used in the near future or expected to be used again. Therefore the register file bank at
this level should possess as many ports as a conventional monolithic register file but fewer entries. On the other 
hand, the register file bank at the lowest level functions as the pool of physical registers. It stores all the 
physical registers (either freed or renamed) including those reside in the register file cache and all the results 
are always written back to this level. This implies a large number of entries. As of the ports between the register
file (lowest level) and the register file cache (uppermost level) will be studied in Chapter \ref{sec:performance}.

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.60]{rfc_diagram.pdf}}
	\caption[A two-level Register File Cache architecture]{A two-level Register File Cache architecture. Source \cite{antonio00}}
	\label{rfc_diagram}
\end{figure}

Since there is a presence of a cache-like organization, there is a need to determine which values are cached and to
predict whether to prefetch a certain value along with the replacement policy. Two caching policies were mentioned 
based on the authors observation of the fact that register values are often used only once \cite{antonio00}. The 
first of which is called \textit{non-bypass} caching policy. In this caching policy only those results not read 
from the bypass network are cached in the register file cache (uppermost level) whereas the values which are 
bypassed are written back only to the register file (lowest level). Another caching policy is \textit{ready} 
caching which we only cache the results that are a source operand of an instruction not yet issued but has all its 
other source operands ready. 
~\\ \\
Orthogonal to the caching policies two fetching mechanisms were also mentioned. In \textit{fetch-on-demand} 
register values are fetched to the register file cache (uppermost level) when they are source operands of some 
instructions which have all their other source operands ready but those reside in the register file (lowest level).
\textit{Prefetch-first-pair} exploits the fact that the rename and issue logic of a conventional processor is able 
to identify all the operand communications between the instructions in the issue-window. Whenever an instruction is
issued, the issue queue will be scanned to find the first instruction that needs the destination operand of the 
current instruction as one of its source operands. The other operands of the same instruction will then be 
prefetched into the register file cache (uppermost level). An example is showing below: \\ \\
\indent \indent \textit{p1 = p2 + p3;} \\
\indent \indent \textit{p4 = p3 + p6;} \\
\indent \indent \textit{p7 = p1 + p8;} 
~\\ \\
The aforementioned architecture forms the base of our work in this thesis since we are expecting a 2-cycle register
file to reach a performance as close as that of an ideal 1-cycle register file. However, because here we are 
tackling with the process variation problem we are going to assume a weak-strong pair. This is to say that even a 
2-cycle register file cannot be free of the process variations. Over time some of the cell of the register file 
will simply become unreachable which effectively reduce the capacity of the register file, hence ``weak''. On the 
other hand, since the register file cache is relatively simple we can make it free of the process variation and 
becomes the ``strong'' part of this pair. Later we will see how well can the register file cache architecture 
dealing with the performance drop brought along by the weak-strong configuration.

