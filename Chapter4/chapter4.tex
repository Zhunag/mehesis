\chapter{Experiments and Evaluations}

% **************************** Define Graphics Path **************************
\ifpdf
    \graphicspath{{Chapter4/Figs/Raster/}{Chapter4/Figs/PDF/}{Chapter4/Figs/}}
\else
    \graphicspath{{Chapter4/Figs/Vector/}{Chapter4/Figs/}}
\fi

\section{Experiment Platform}
\label{sec:performance}

The performance was evaluated using a cycle-accurate microarchitectural simulator: MARSSx86 \cite{marssx86}. It 
simulates a modern single-/multi-core x86 processor (both in-order and out-of-order). The out-of-order mode 
(the one used in this thesis) incorporates an issue queue, a RAT (Register Aliasing Table), physical register 
files for both integer and floating point registers. It implements two ROBs (Re-Order Buffer), a speculative ROB 
which resides at the renaming stage and another ROB which resides at the commit stage and is used to recover from 
branch mispredictions and interrupts. Figure \ref{marss_diagram} illustrates the execution flow of MARSSx86 
whereas Figure \ref{marss_pipeline} depicts the pipeline stages of the out-of-order core model used in 
the simulation. 

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.50]{block_diagram.png}}
	\caption[The block diagram of the Marssx86 structure]{The block diagram of the Marssx86 structure. Source \cite{marssx86}}
	\label{marss_diagram}
\end{figure}

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.45]{marss1.png}}
	\caption[Pipeline stages of the out-of-order core model]{Pipeline stages of the out-of-order core model. Source \cite{marssx86}}
	\label{marss_pipeline}
\end{figure}
~\\ \\
It models a 6-stage pipeline (instruction fetch; decode; rename; issue; execute; write-back; commit). The source 
operands are read in the same stage as it is issued. All the stages takes exactly one cycle except for execute 
stage which can take several cycles depends on the instructions. We modified the relevant parts of the simulator 
in order to properly simulate the register file cache architecture. Table \ref{table:parameters} lists the major 
microarchitecture features we used throughout all the experiments. We implemented the register file cache 
(uppermost level) as a fully-associative organization with an LRU replacement policy. As of the caching policy and 
fetching mechanism we chose \textit{non-bypass} caching and \textit{prefetch-first-pair} fetching because this 
combination achieved the highest IPC according to \cite{antonio00}. \\

\begin{table}[bhtp]
	\begin{tabular}{|c|p{9cm}|}
		\hline 
		\textbf{Parameter} & \textbf{Value} \\ 
		\hline \hline
		$\mu$ops operands & 1 destination operand and up to 3 source operands (source operand can be either register or immediate value) \\
		\hline
		Fetch width & 4 instructions \\ 
		\hline 
		I-cache & 128 KB, 8 way-associative, 64 byte lines, 2-cycle hit time, 5-cycle miss time \\ 
		\hline 
		Branch predictor & Combined (Gshare with 2-bit bi-modal) \\ 
		\hline 
		Reorder buffer size & 128 \\
		\hline
		Issue queue size & 64 \\ 
		\hline 
		Functional units & 4 int, 4 FP, 4 load\slash store \\ 
		\hline 
		Load\slash store queue & 64 \\ 
		\hline 
		Issue mechanism & 4-way out-of-order issue \\ 
		\hline 
		Physical registers & 256 int \slash 256 FP \\ 
		\hline 
		D-cache & 128 KB, 8 way-associative, 64 byte lines, 2-cycle hit time, 5-cycle miss time \\ 
		\hline 
		Commit width & 4 instructions \\ 
		\hline 
		Register file access time & 2 cycles \\
		\hline
		Register file cache access time & 1 cycle \\
		\hline
	\end{tabular}
	\caption{Microarchitecture parameters used for simulation}
	\label{table:parameters}
\end{table}

For the evaluation of the register file cache architecture, we ran the SPEC CPU 2006 benchmarks \cite{spec_website}
on the MARSSx86 simulator. SPEC CPU 2006 is the latest version of a series of benchmarks designed to provide a 
comparative measure of compute-intensive performance across the widest practical range of hardware using workloads 
developed from real user applications. It contains two benchmark suites: INT which is composed of 12 benchmarks and
measures the compute-intensive integer performance; FP which is composed of 17 benchmarks and measures the 
compute-intensive floating point performance \cite{spec_art}. All the results presented in this thesis are based 
upon the IPC (instruction per cycle) and other relevant hardware counter data from the execution of the benchmarks. 
~\\ \\
Since the SPEC CPU 2006 benchmarks have significantly larger dynamic instruction counts and data footprint than the
earlier SPEC CPU 2000 benchmarks \cite{spec_profile}, a full execution of the benchmarks on the simulator would 
take days even weeks to complete. In order to reduce the execution time while retain the accuracy of the simulation
result we used some profiling and instrumentation techniques. We used Intel\textregistered PinPoints 
\cite{pinpoints} to analyse and dynamically instrument the benchmarks. Consequently, it generates a BBV (basic 
block vector) file of the instrumented benchmark which we then fed it to SimPoint \cite{simpoint}. Given the 
instruction slice (number of instructions) SimPoint will in turn pick up the most representative instruction 
slices and provide the weight of each of them. In this thesis the instruction slice we used is 250 million which is
reported to be relatively accurate \cite{spec_profile}. 

\section{Impact of Process Variation}

The constant presence of process variations renders the register file itself unstable. Cells of the register file 
could be temporarily malfunctioning or even broken over time thus reduces the register file size temporarily or 
permanently. Figure \ref{rf_rfc} illustrates the impact of the degradation of the register file on the performance.
Here we assume a weak-strong pair architecture, the register file size degrades whereas the register file cache 
retains its original size. Also we introduce two baselines, a best-case scenario (an ideal 1-cycle monolithic 
register file) and a worst-case scenario (a 2-cycle monolithic register file with only one level of bypass network)
. We used four different sizes of the register file cache, we will devote the next section analysing their impacts 
on the performance.

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.70]{rf_rfc_int.pdf}}
	\centerline{\includegraphics[scale=0.70]{rf_rfc_fp.pdf}}
	\caption{We assume a strong register file cache with a weak register file}
	\label{rf_rfc}
\end{figure}

Not surprisingly, we observe a performance drop whenever the register file size decreases in both SPEC CPU 06 INT 
and FP benchmark suites. The performance degradation is within 5\% after the register file size decreases to 96 
entries in the INT benchmark suite. For FP the degradation is within 9\%. As the register file size continues to 
decrease the performance curves become steeper because the simulator incorporates 80 physical registers, as the 
size of the register file is approaching down to or even below this number the register file would be getting 
difficult to serve all the requests from the issue queue on time. 
~\\ \\
We also observe that despite some minor fluctuations, among the four sizes of the register file cache the 
performance curves see a rather similar pattern. In the INT benchmark suite the register file cache architecture 
sees a greater degradation (2\% - 3\%) of performance than an ideal 1-cycle monolithic register file when the 
register file size is somewhere around 64 to 96 entries. However, the performance curves converge when the register
file size keeps increasing or decreasing. Whereas in FP benchmark we do not see this pattern, all the curves follow
a quite close if not identical pattern. 

\section{Impact of the RFC size}

Similar to a conventional cache memory system the size of a register file cache is critical to achieving high 
performance. Figure \ref{cache_size} shows the performances of various register file cache sizes (8, 16, 24, 32 
entries) compare to an ideal 1-cycle monolithic register file. We introduce another parameter here: the buses 
between the register file and the register file cache which we will discuss in the next section. Regardless of the 
number of buses in-between these two register file banks, there is a performance gain as the register file cache 
size increases. However, as we can observe in the Figure \ref{cache_size} the trend flattens when the register file
cache size approaches 32 entries which implies a performance bottleneck of the register file cache architecture. 
~\\ \\
Overall the register file cache architecture incurs performance lose of around 3\% - 10\% in SPEC CPU 2006 INT 
benchmark suite depending on the register file cache size compare to an ideal 1-cycle register file as well as the 
buses in-between. Nevertheless, it still possesses a 3\% - 10\% speed-up over a 2-cycle register file with one 
bypass level in both benchmark suites.

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{2bus_int.pdf}
		\includegraphics[scale=0.50]{2bus_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{4bus_int.pdf}
		\includegraphics[scale=0.50]{4bus_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{8bus_int.pdf}
		\includegraphics[scale=0.50]{8bus_fp.pdf}
	}
	\caption{Performance of various register file cache sizes}
	\label{cache_size}
\end{figure}
\newpage

\section{Usage of The Buses}

In the previous section, we have seen that regardless the number of available buses between the register file and 
the register file cache the performance trends are nearly identical. This leads us to a further check. The results 
show in Figure \ref{bus0} and \ref{bus}. Obviously the performance of deploying 4 and 8 buses are nearly identical 
regardless the register file cache size while with 2 buses the performance sees a minor drop with the register file
cache size of 8 and 16. However, a check on the actual statistic reveals that the performance drop is not 
significant, less than 2\%. Hence, of all the register file cache size we have experimented the impact of using 
either 2, 4 or 8 buses is not apparent which implies the rare usage of the buses.

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{8entries_int.pdf}
		\includegraphics[scale=0.50]{8entries_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{16entries_int.pdf}
		\includegraphics[scale=0.50]{16entries_fp.pdf}
	}
	\caption{Impact on performance of the number of buses between the RF and the RF Cache}
	\label{bus0}
\end{figure}

\begin{figure}[hbtp]
	\centerline{
		\includegraphics[scale=0.50]{24entries_int.pdf}
		\includegraphics[scale=0.50]{24entries_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{32entries_int.pdf}
		\includegraphics[scale=0.50]{32entries_fp.pdf}
	}
	\caption{The buses in-between have minor impact on performance (continue.)}
	\label{bus}
\end{figure}
\newpage
We gathered the bus usage information in every cycle during the simulation (shown in Figure \ref{ports_usage}). Due
to the presence of the prefetching mechanism on top of the fetch-on-demand fetching policy, 2 buses are barely 
enough to meet the read requests to the register file cache. It is especially prominent for small register file 
caches. We can see in Figure \ref{ports_usage} that with a register file cache size of 8 and 2 buses these 2 buses 
are quite busy for more than 60\% of the cycles in the SPEC CPU 2006 INT benchmark suite. Naturally, the bus 
demands reduces with the increasing of the register file cache size since it is able to hold more registers at a 
time. When 4 or 8 buses are incorporated the cycles where there is no traffic becoming more and more prominent up 
to close to 50\% for the SPEC CPU 2006 INT benchmark suite and around 60\% for the SPEC CPU 2006 FP benchmark suite
. This somehow explains why in these 2 bus configurations the number of buses does not matter any more. 

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{ports_usage_2_int.pdf}
		\includegraphics[scale=0.50]{ports_usage_2_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{ports_usage_4_int.pdf}
		\includegraphics[scale=0.50]{ports_usage_4_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{ports_usage_8_int.pdf}
		\includegraphics[scale=0.50]{ports_usage_8_fp.pdf}
	}
	\caption{Bus usage with various bus numbers}
	\label{ports_usage}
\end{figure}

\newpage
\section{Usage of The Bypass Network}
As we have seen in the last section, the bus traffic between the register file and the register file cache does not
seem to be heavy, two buses is sufficient to achieve a relatively high performance with the maximum performance 
drop of less than 2\%. This could imply a frequent use of the bypass network. The percentage of use of the bypass 
network under various number of buses is shown in Figure \ref{bypass_usage}. First, we can observe that the number 
of buses has minor effect on the percentage of the usage of the bypass network. In SPEC06 INT benchmark suite of 
all the execution cycles around 50\% of which the bypass network is not used. This number decrements when the 
register file cache size becomes larger. On the other hand, a one-bypass usage takes place in around 34\% of all 
the execution cycles, it slightly increments corresponding to the increase of the register file cache size. The 
two-bypass and three-bypass usage, which occur less frequently, are 12\% and 2\% respectively. 

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{bypass_2bus_int.pdf}
		\includegraphics[scale=0.50]{bypass_2bus_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{bypass_4bus_int.pdf}
		\includegraphics[scale=0.50]{bypass_4bus_fp.pdf}
	}
	\centerline{
		\includegraphics[scale=0.50]{bypass_8bus_int.pdf}
		\includegraphics[scale=0.50]{bypass_8bus_fp.pdf}
	}
	\caption{Bypass usage with various bus numbers}
	\label{bypass_usage}
\end{figure}
\newpage

Given the frequent bypass network traffic, we set out to study its relationship with the $\mu$ops being executed. 
We distinguish the $\mu$ops by the number of source operands they have since the number of the source operands has 
direct impact on the bypass traffic. As in Table \ref{table:parameters}, each $\mu$ops in can have up to 3 source 
operands, we thus divide the $\mu$ops into 4 categories. Figure \ref{uops_usage} shows the harmonic mean of the 
percentage of the $\mu$ops of each category from both the SPEC06 INT and FP benchmark suites. We can observe that 
the $\mu$ops with 2 source operands are dominant (69\% in SPEC06 INT and 67.5\% in SPEC06 FP) compare to others. 
In Table \ref{table:uops} we list some typical $\mu$ops from each type of the category in hope for a better 
understanding of Figure \ref{uops_usage}:
~\\ \\
From Table \ref{table:uops}, we can see that most of the commonly used $\mu$ops has 2 source operands which 
explains the dominantly high percentage of 2-source-operand $\mu$ops from Figure \ref{uops_usage}. \\
\begin{table}[bhtp]
	\begin{tabular}{|c|p{10cm}|}
		\hline 
		\textbf{Category} & \textbf{Typical $\mu$ops} \\ 
		\hline \hline
		$\mu$ops with no source operand & unconditional branch (\emph{bru}), \emph{nop}\\
		\hline
		$\mu$ops with 1 source operand & conditional branch (\emph{br.cc}), indirect jump (\emph{jmp}, \emph{jmpp}), FP convertion (\emph{cvtf})\\
		\hline 
		$\mu$ops with 2 source operands & \emph{mov}, logical (\emph{and}, \emph{xor} etc.), integer/FP arithmetic (\emph{add}, \emph{mul}, \emph{addf}, \emph{mulf} etc.), load (\emph{ld} etc.), shift (\emph{shl}, \emph{shr}), rotate (\emph{rotl}, \emph{rotr}), \emph{mask}, bit testing (\emph{bt}, \emph{btr}, etc.), \emph{cmpf}\\
		\hline 
		$\mu$ops with 3 source operands & store (\emph{st} etc.), conditional set/select (\emph{set.cc}, \emph{sel.cc}),  conditional compare and set (\emph{set.sub.cc}, \emph{set.and.cc}), \emph{collcc}\\
		\hline 
	\end{tabular}
	\caption{Typical $\mu$ops classified by the number of operands}
	\label{table:uops}
\end{table}

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{uops_int.pdf}
		\includegraphics[scale=0.50]{uops_fp.pdf}
	}
	\caption{Distribution of $\mu$ops with different numbers of source operands}
	\label{uops_usage}
\end{figure}

We also studied how do 2-source-operand $\mu$ops get their source operand values (either from the register file 
cache or from the bypass network) and the result is shown in Figure \ref{uops_2operands}. As indicated in Figure 
\ref{bus} the buses has minor effect on the performance plus the statistic of different buses configurations does 
not differ much, we hence use the smallest number of buses possible (2 buses) since the buses can significantly 
increase the overall area and power. In the SPEC06 INT benchmark suite, the situation that the $\mu$ops read both 
their source operand values from the register file is around 45\% and this figure decrements as the register file 
cache size increases. In the SPEC06 FP the percentage is 6\% higher. This is not particularly surprising, given 
that the majority of the $\mu$ops has 2 source operands, the result in Figure \ref{uops_2operands} should reflect 
the overall trends shown in Figure \ref{bypass_usage}.

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{uops_2operands_int_2bus.pdf}
		\includegraphics[scale=0.50]{uops_2operands_fp_2bus.pdf}
	}
	\caption{The distribution of sources where 2-source-operand $\mu$ops get their source operand values}
	\label{uops_2operands}
\end{figure}

We also would like to know the overall performance of the issue queue under the influence of the register file 
cache architecture by finding out the percentage of $\mu$ops failed being issued due to the absence of their 
operands values in the register file cache. However, there are several factors that can prevent a given $\mu$op 
from being issued (operand not ready, no available function units, already reach the issue width etc.). In order 
to rule out the rest of the factors, we modified the simulator to gather the following information: the number of 
$\mu$ops which are within the issue width and meet the \emph{fetch-on-demand} policy but do not have all the 
values of their operands available in the register file cache. The result is presented in Figure \ref{iq_wait}. We 
can see that in the SPEC06 INT benchmark suite for a register file cache size of 8 entries, the percentage is 
around 26\% and this number decreases when the capacity of the register file cache increments. Since the register 
file cache is able to hold more and more values, this becomes obvious. The trend of this flattens which indicates 
a possible bottleneck of the register file cache size. Similar percentage and tendency can also be observed in the 
SPEC06 FP benchmark suite. 

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{absence_int.pdf}
		\includegraphics[scale=0.50]{absence_fp.pdf}
	}
	\caption{The percentage of $\mu$ops that could not be issued due to the absence of their operands values in the register file cache}
	\label{iq_wait}
\end{figure}

\section{Power and Area Evaluation}
\label{sec:power_area}
Apart from studying the performance impact of the register file cache, we evaluated its impact on the power 
consumption and the area overhead as well. We chose the McPAT \cite{mcpat} from HP Labs as the evaluation 
framework. McPAT is an integrated power, area and timing modelling framework for multi-threaded, 
multi-core/many-core processors. It provides a set of complete hierarchical models from architecture to the 
technology level by the means of an XML-based interface.

Since the process variation has become and is predicted to be one of the greatest threaten to integrated circuits, 
we chose the model of Intel Xeon processor of 22 nm technology during the evaluation which is the most recent 
technology node available in the current version of McPAT. Marssx86 has support to McPAT by the means of properly 
dumping the simulation results to McPAT's XML interface. The only thing we modified is the register file read and 
writing statistics. The lowest-level register file is read when the data from it is brought to the register file 
cache whereas every destination operand value is written back to it in the writeback stage. On the other hand, 
every read request of the operations from the issue queue is a read operation to the register file cache and the 
write operation to it is a combination of both the data transfer between the register file and the register file 
cache and the destination operand values in the writeback stage that are not read by the bypass network. 

Figure \ref{area} is a comparison of both the overall processor area (mm$^2$) and the area of the register file 
among 4 different register file cache sizes and an ideal 1-cycle register file as well as a 2-cycle 1-bypass 
register file. Since we are adding up components on top of the original processor it is reasonable to expect an 
overhead in terms of the area and the larger the register file cache size is the larger the area overhead is. We 
assume that to make the register file more reliable the transistor that the 2-cycle 1-bypass register file made of 
would be slightly larger. We suppose it would take 30\% more space than that of an ideal 1-cycle register file. We 
can see from Figure \ref{area} that the register file caches still take less area than a 2-cycle 1-bypass register 
file. 

A register file cache architecture implies more transistors over a conventional register file and it  inevitably 
increases the power consumption. Our result shows that the register file cache architecture incurs an overhead 
of 0.4\% and generally the overhead is positively correlated to the register file cache size.

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{area_overall.pdf}
		\includegraphics[scale=0.50]{area_rf.pdf}
	}
	\caption{Area of the processor and register file with/without register file cache}
	\label{area}
\end{figure}

We further made an energy delay square metric diagram shown in which the numbers are relative to the result from 
the 2-cycle 1-bypass register file (Figure \ref{energy_delay}). It shows that in terms of energy efficiency the 
register file cache size of 8, 16, 24, 32 possess a rather similar efficiency (around 72\% - 79\% compare to the 
2-cycle 1-bypass register file). Without any doubt the ideal 1-cycle register file achieves is 33\% more 
energy-efficient than a 2-cycle 1-bypass register file. 

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.60]{energy_delay.pdf}}
	\caption{The Energy-Delay Square metrics}
	\label{energy_delay}
\end{figure}

\newpage
\section{Architectural Vulnerability Factor (AVF) of the RFC}

\subsection{Architectural Vulnerability Factor (AVF)}

In \cite{avf}, the authors recognize the soft error as a major challenge for the microprocessor designers. They 
listed some of the existing techniques(special radiation-hardened circuit designs, localized error detection and 
correction, architectural redundancy, etc.) in hope to reduce the impact of the soft error. Nevertheless, 
they argue, that all of these approaches introduce a significant penalty in performance, power, die size, and 
design time. Although a microprocessor with inadequate protection from transient faults may prove useless due to 
its unreliability, excessive protection may make the resulting product uncompetitive in cost and/or performance.
Plus, tools and techniques to estimate processor transient error rates are not yet available or mature enough.
~\\ \\
Instead, they proposed to use the probability that a fault in a processor structure will result in a visible error 
in the final output of a program that structure's \emph{architectural vulnerability factor} (AVF). They reasoned 
that the key to generating these error-rate estimates is understanding that not all faults in a microarchitectural
structure affect the final outcome of a program. As a result, an estimate based only on raw device fault rates will
be pessimistic, leading architects to over-design their processor's fault-handling features \cite{avf}.
~\\ \\
To give an example they argued that the branch predictor’s AVF is 0\%. In contrast, a single-bit fault in the 
committed program counter will cause the wrong instructions to be executed, almost certainly affecting the 
program's result. Hence, the AVF for the committed program counter is effectively 100\% \cite{avf}. These are two 
extreme cases, it is obvious that the AVF for many other microarchitectural components will be some where between 
these two.
~\\ \\
They think that by incorporating AVF into the early-stage design a processor architect can map the raw fault rate 
(dictated by process and circuit issues) to an overall processor error rate, and thus determine whether the design 
meets its error rate goals (set according to the target market). Significantly, this allows an architect to examine
the relative contributions of various structures and identify the most cost-effective areas to employ fault 
protection techniques.

\subsection{AVF for the RFC}

The way the authors in \cite{avf} propose to compute the AVF for one microarchitectural component is to track the 
processor state bits required for \emph{architectural correct execution} (ACE). Any fault in a storage cell that 
contains one of these bits, which we call ACE bits, will cause a visible error in the final output of a program in 
the absence of error correction techniques. As can be easily deduced, a fault on an un-ACE bit will not incur an 
error in the execution.

\subsubsection*{Identify the ACE bits}
For the sake of a general discussion, the authors in \cite{avf} listed various criteria on identifying an ACE bit 
in a microarchitectural component. However, the scope of the thesis focuses on the register file and its attached
cache-like architecture. Identifying the ACE bits becomes a rather straightforward task.
~\\ \\
As register file provides the actual data to each and every instruction in the pipeline, in this thesis we consider
all of them critical to the correct execution. Nevertheless, the value of a register only becomes valid when the
physical register is assigned to an architectural register. We take that into account and only consider valid 
registers in the following experiments. As an attachment to the register file, the methodology also apply to the 
RFC.

\subsubsection*{Computing AVF}
The AVF of a storage cell is the fraction of time an upset in that cell will cause a visible error in the final 
output of a program. Thus, the AVF for an unprotected storage cell is the percentage of time the cell contains an 
ACE bit. To extend it to an entire hardware structure: The AVF for a hardware structure is simply the average AVF 
for all its bits in that structure, assuming that all bits in that structure have the same circuit composition and,
hence, the same raw FIT rate. Then, the AVF of a hardware structure is equal to: \\ \\
\emph{average number of ACE bits resident in a hardware structure in a cycle} \\
-------------------------------------------------------------------------\\
\emph{total number of bits in the hardware structure} \\ \\
or, \\ \\
$\Sigma$ \emph{residency (in cycles) of all ACE bits in a structure} \\
--------------------------------------------------------------------------------- \\
\emph{total number of bits in the hardware structure × total execution cycles} \\
~\\
Another method is to use Little's Law: \\
\textbf{N = B×L}\\
where N = \emph{average number of bits in a box}, B = \emph{average bandwidth per cycle into the box}, 
and L = \emph{average latency of an individual bit through the box}. \\ \\
The AVF of the structure can then be expressed as: \\ \\
\indent $B_{ace}$ X $L_{ace}$ \\
------------------------------------------------\\
\emph{total number of bits in the hardware structure} \\ 
~\\
In this thesis, we use the $2^{nd}$ equation to compute AVF for the RFC. What needed are: \\
\begin{itemize}
	\item sum of all residence cycles of all valid RFC entries during the execution of the program,
	\item total execution cycles for which we observe the ACE bits' residence time,
	\item total number of bits in the RFC (each entry is composed of 64 bits for a x86-64 family processor).
\end{itemize}

\subsubsection*{Experiments and Results}

The aforementioned AVF is a relatively accurate metric to the designers than mere guard-baning. We would like to compute the AVF
for the RFC architecture by incorporating the MARSSx86 simulator with the ability to track the  physical register assignment. 
In order to setup a baseline, we first ran a set of experiments in computing the AVF of a traditional monolithic 1-cycle register 
file (Figure \ref{avf_orig}). Since the access patterns of the integer register file and the floating-point register file differ 
in a integer-dominated and floating-point-dominated benchmark we separated AVF of these two register files.
~\\ \\
We can observe that in both the integer (INT) and floating-point (FP) benchmark the AVF of the integer register file is generally
higher than that of the floating-point register file. Though the AVF of the integer register file are both around 50\%, the
AVF of the floating-point one, however, is higher in the FP benchmark. The reason is apparent since in the FP benchmark the 
floating-point register file is heavily used and thus has more ACE bits than in the INT benchmark.

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{avf_original_int.pdf}
		\includegraphics[scale=0.50]{avf_original_fp.pdf}
	}
	\caption{The AVF of a monolithic 1-cycle register file}
	\label{avf_orig}
\end{figure}

~\\ \\
Figure \ref{avf_rfc} depicts the AVF of the RFC architecture in various sizes. Compare to the AVF of the monolithic 1-cycle register
file, the AVF of the RFCs are generally higher (up to 80\% in the INT benchmark and 90\% in the FP benchmark for the integer 
register file). This is because unlike the monolithic register file, only the register values recently assigned are brought to the 
RFC which means the entries in RFCs are more probable to contain ACE bits.
As a general trend when the size of the RFC becomes bigger the AVF decreases. One explanation is that as the size of the RFC 
increases the replacement (LRU) becomes less frequent which results in more un-ACE bits resides in the RFC.

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{AVF_INT.pdf}
		\includegraphics[scale=0.50]{AVF_FP.pdf}
	}
	\caption{The AVF of the RFCs}
	\label{avf_rfc}
\end{figure}
\newpage

\subsection{Soft Error Handling By The RFC}

The register file cache architecture is essentially a partial redundant component of a traditional register file which can be used 
to handle the SEU. We would like to evaluate how the SEUs impose an influence on the RFC architecture. Our mechanism for the RFC 
architecture to handle SEU is the following: \\ \\
If an SEU occurs, the affected $\mu$op is marked as ``re-issue'' and being sent back to the issue queue where it was once were. 
All the succeeding $\mu$ops that have dependencies on that one will be hold in the issue queue as are normally handled while the 
faulty cell block will be marked as ``invalid'' and all the corresponding value will be read directly from the register file itself
(which takes 2-cycles). The faulty cell block will be ``fixed'' when the value is flushed and the new value from the register file
will be correct again.
~\\ \\
For the simulation, apart from embedding this mechanism into the simulator we also needed to change the minimum number of buses
from the previous 2 buses to 3 buses, because in extreme cases where all the 3 operands of one $\mu$op are hit by the soft error 
then these 3 operands needs to reach the RFC in the same time or a possible dead-lock might be created. To simulate the random 
nature of the soft error we modified the simulator to randomly inject soft errors in every certain cycles. As a baseline, Figure 
\ref{rfc_3bus} presents the performance results of the 3-bus RFC. Figure \ref{rfc_3bus_seu} shows the performance of a 3-bus RFC
with soft errors injected every 10 cycles. \\ \\

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.70]{rfc_3bus_int.pdf}}
	\centerline{\includegraphics[scale=0.70]{rfc_3bus_fp.pdf}}
	\caption{Performance of 3 buses without SEUs}
	\label{rfc_3bus}
\end{figure}

\begin{figure}[bhtp]
	\centerline{\includegraphics[scale=0.70]{rfc_3bus_seu_int.pdf}}
	\centerline{\includegraphics[scale=0.70]{rfc_3bus_seu_fp.pdf}}
	\caption{Performance of 3 buses with SEUs}
	\label{rfc_3bus_seu}
\end{figure}
\newpage

We can observe that the performance gap between Figure \ref{rfc_3bus} and Figure \ref{rfc_3bus_seu} is minimal, less than 2\%. With
an injection rate of SEU/10 cycles the result is quite surprising. We conducted another set of experiments to find out the reason.
As shown in Figure \ref{rfc_seu} when a cell block in the RFC is stricken (hit by an SEU) only less than 25\% of such blocks were 
read again afterwards and most of them were not used again until being replace by a new and correct value. 

\begin{figure}[bhtp]
	\centerline{
		\includegraphics[scale=0.50]{SEU_INT.pdf}
		\includegraphics[scale=0.50]{SEU_FP.pdf}
	}
	\caption{RFC Read after SEUs}
	\label{rfc_seu}
\end{figure}

