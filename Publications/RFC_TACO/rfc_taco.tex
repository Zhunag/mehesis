\documentclass[prodmode,acmtaco]{acmsmall}


\usepackage{mathtools}
\usepackage{enumitem}
% Metadata Information
%\acmVolume{9}
%\acmNumber{4}
%\acmArticle{39}
%\acmYear{2010}
%\acmMonth{3}

% Document starts
\begin{document}

% Page heads
\markboth{S.Zhuang, J-L.Cruz and R.Canal}{A Reliability-Aware Register File Cache Architecture to Mitigate the Impact of Process Variations}

% Title portion
\title{A Reliability-Aware Register File Cache Architecture to Mitigate the Impact of Process Variations}
\author{
Sicong Zhuang, Jos\'{e}-Lorenzo Cruz and Ramon Canal\affil{Universitat Polit\`{e}cnica de Catalunya}
}

\begin{abstract}
Under current technologies process variation has posed a great threat to the SRAM stability and yield. It is expected to worsen with the constant shrinking-down of the transistor size. Using bigger SRAM cells can mitigate this to some extent. However, a big SRAM cell can result in a lower performance and increases the overall area. In this paper, we propose to use a multi-level register file cache architecture to reduce the penalty of a large register file. The results show a 3\% - 11\% performance drop compare to an ideal 1-cycle variation-free register file but a 3\% - 10\% performance gain over a more realistic 2-cycle register file. We also conduct a thorough study on how the size of the register file cache as well as the buses between the register file and the register file cache affect the performance on a modern x86 processor. We evaluate the overall power (Watt) and area (mm$^2$) of this register file architecture as well. We show that the register file cache incurs an area overhead of 0.2\% and 0.4\% the overhead of the power consumption. 
\end{abstract}

\category{C.1}{Processor Architectures}{General}

\terms{Experimentation, Measurement, Design}

\keywords{register file, process variation, SRAM}

\acmformat{Sicong Zhuang, Jos\'{e}-Lorenzo Cruz and Ramon Canal, 2013. A Reliability-Aware Register File Cache Architecture to Mitigate the Impact of Process Variation.}

%\begin{bottomstuff}
%\end{bottomstuff}
\maketitle

\section{Introduction}
\label{sec:introduction}
Variations in process parameters affect the digital integrated circuit (IC) and they have become a critical issue faced by the IC designers. Such variations are results of not only the fluctuations during fabrication (length, width, oxide thickness etc.) but also some aging-related variations (NBTI, PBTI, HCI etc.). The consequence is that they make the integrated circuits behave non-deterministically in terms of power consumption, stability and maximum speed degradation. This is especially critical for SRAM arrays as they are usually implemented with small transistors to increase density. As the device geometries continue to shrink and fabrication technology scales beyond 32 nm process variations are expected to be an even greater obstacle. \cite{survey} \cite{jaksic}

On the other hand, most current superscalar microprocessors use a RISC-like instruction set architecture (ISA). Such ISA implies that majority of the instruction operands reside in the register file. At the same time, high-end superscalar microprocessors that use out-of-order instruction processing have relied on the use of bigger and bigger register files to expose and exploit instruction level parallelism (ILP). 

The increasing threat from process variations will certainly affect the  the register file in the current and future technologies rendering it unreliable; and consequently, harm the performance of the entire processor. One countermeasure is to use guard-banding by deploying sufficiently large SRAM cells in the register file in order to minimize the impact of process variation. However, this will increase the register file area and, caused by the longer wires, impact negatively the access time. Albeit several techniques are available to increase the robustness of any SRAM array (e.g. WL boosting, redundancy, ECC, etc), in this paper we go one step further. We want to evaluate the effectiveness of a microarchitecutral technique to combat process variations in the register file.

In order to quantify the impact on performance of the register file size, we conducted an experiment on a cycle-accurate x86-family simulator. We used a set of unified microarchitectural parameters during the entire paper. A brief introduction of the characteristics of the simulator and a detailed description of the microarchitectural parameters we used is given in Section \ref{sec:performance}. 

First, we quantify the performance hit caused by the reduction of available physical registers in the register file. The result is shown in Figure \ref{physreg_comparison} (Register file size refers to the size of both the integer physical register file and the FP physical register file). In both benchmark suites (INT and FP) a larger register file size leads to a gain in performance. Nevertheless, to some points (somewhere between 128 and 192 entries) the performance flattens. We can observe that the performance of a register file of 192 entries has no performance difference compared to a register file of 256 in both benchmark suites. This leads to an ideal size of a register file size in a modern out-of-order x86 microprocessor (see Table \ref{table:parameters} for more configuration details) in terms of performance, namely, somewhere around 192 and 256. A register file of such size with 8 read ports and 4 write ports can hardly achieve a one cycle access time and be 100\% process variation resistant.


\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{physreg_comparison_int.pdf}
\includegraphics[scale=0.50]{physreg_comparison_fp.pdf}
}
\caption{Performance with various register file size}
\label{physreg_comparison}
\end{figure}


In order to mitigate the performance drop introduced by a register file with more-than-one-cycle access time, instead of enlarging the SRAM cells, we propose to use the register file cache architecture originally proposed in \cite{antonio00}. This rest of the paper is organized as following: We describe the register file cache architecture in detail in Section \ref{sec:rfc_architecture}. The performance and the reliability-related issue will be discussed in Section \ref{sec:performance}. We evaluate the overall area and the consumed power of this architecture in Section \ref{sec:power_area}. Related work is presented in Section \ref{sec:related_works} and finally we draw the conclusion in Section \ref{sec:conclusion}.

\section{Register File Cache Architecture}
\label{sec:rfc_architecture}
The need of a large register file size of a modern out-of-order processor along with the requirement to implement a register file using large SRAM cells to minimize the impact of process variations leads to an overprovisioned register file. The register file provides the source operands and stores the results of most instructions. The register renaming mechanism of a dynamically scheduled processors rename logical registers to physical registers at run time such that each result produced by any instruction in-flight is allocated to a different physical register. Under this scenario, the register file access time could be critical. Compare to an ideal 1-cycle register file, a register file with 2-cycle access time 1) resolves branches one cycle later 2) requires an extra level of the bypass network which increases the complexity significantly.

At the expense of a lower performance, the complexity of the bypass network of a 2-cycle register file can be reduced by only implementing one level of the bypass network. Figure \ref{2cycle} shows the performance comparison between an ideal 1-cycle register file and a 2-cycle register file in which only one level of bypass is implemented. We chose to keep the bypass network from the last level (the second cycle of the 2-cycle writeback stage) because otherwise there would be an undesirable situation in which a value is available in one cycle via the bypass network and becomes inaccessible in the subsequent cycle then can be accessed via the register file in the next cycle. This would greatly increase the complexity of the issue logic. 
\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.70]{2cycle_int.pdf}}
\centerline{\includegraphics[scale=0.70]{2cycle_fp.pdf}}
\caption{Performance drop of a 2-cycle 1-bypass level register file compare to an ideal 1-cycle register file}
\label{2cycle}
\end{figure}
In Figure \ref{2cycle}, we can observe an average performance drop of around 13\% for a 2-cycle 1-bypass-level register file compared to an ideal 1-cycle register file (we used harmonic mean) in both SPEC06 INT and SPEC06 FP benchmark suites. This is quite a significant performance lose due to the larger register file access time and the lack of the full bypass network implementation.

While originally proposed to cope with big (and slow) register files, we reevaluate the register file cache \cite{antonio00} as a mechanism to tolerate and hide the effect of process variations in the register file. The original proposal consists of a two-level cache-like register file architecture. Figure \ref{rfc_diagram} is an illustration of the aforementioned architecture. It is a heterogeneous design in the sense that there is a register file bank at each level and the two register file banks vary in terms of entries which in turn leads to a different access time. 

As shown in Figure \ref{rfc_diagram}, the levels in the register file cache are named as uppermost level and lowest level. The register file bank at the uppermost level serves as the ``cache'' in this architecture. Hence its name, register file cache. It provides register values required by the instructions on-the-fly and stores register values which are predicted to be used in the near future or expected to be used again. Therefore the register file bank at this level should possess as many ports as a conventional monolithic register file but fewer entries. On the other hand, the register file bank at the lowest level functions as the pool of physical registers. It stores all the physical registers (either freed or renamed) including those reside in the register file cache and all the results are always written back to this level. This implies a large number of entries. As of the ports between the register file (lowest level) and the register file cache (uppermost level) we will study it in Section \ref{sec:performance}.

\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.60]{rfc_diagram.pdf}}
\caption{A two-level Register File Cache architecture}
\label{rfc_diagram}
\end{figure}

Since there is a presence of a cache-like organization, there is a need to determine which values are cached and to predict whether to prefetch a certain value along with the replacement policy. Two caching policies were mentioned based on the authors observation of the fact that register values are often used only once \cite{antonio00}. The first of which is called \textit{non-bypass} caching policy. In this caching policy only those results not read from the bypass network are cached in the register file cache (uppermost level) whereas the values which are bypassed are written back only to the register file (lowest level). Another caching policy is \textit{ready} caching which we only cache the results that are a source operand of an instruction not yet issued but has all its other source operands ready. 

Orthogonal to the caching policies two fetching mechanisms were also mentioned. In \textit{fetch-on-demand} register values are fetched to the register file cache (uppermost level) when they are source operands of some instructions which have all their other source operands ready but those reside in the register file (lowest level). \textit{Prefetch-first-pair} exploits the fact that the rename and issue logic of a conventional processor is able to identify all the operand communications between the instructions in the issue-window. Whenever an instruction is issued, the issue queue will be scanned to find the first instruction that needs the destination operand of the current instruction as one of its source operands. The other operands of the same instruction will then be prefetched into the register file cache (uppermost level).

The aforementioned architecture forms the base of our work in this paper since we are expecting a 2-cycle register file to reach a performance as close as that of an ideal 1-cycle register file. However, because here we are tackling with the process variation problem we are going to assume a weak-strong pair. This is to say that even a 2-cycle register file cannot be free of the process variations. Over time some of the cell of the register file will simply become unreachable which effectively reduce the capacity of the register file, hence ``weak". On the other hand, since the register file cache is relatively simple we can make it free of the process variation and becomes the ``strong" part of this pair. Later we will see how well can the register file cache architecture dealing with the performance drop brought along by the weak-strong configuration.


\section{Performance Evaluation}
\label{sec:performance}
\subsection{Experiment Platform}
We evaluate the performance using a cycle-accurate microarchitectural simulator: MARSSx86 \cite{marssx86}. It simulates a modern single-/multi-core x86 processor (both in-order and out-of-order). The out-of-order mode (the one we use in this paper) incorporates an issue queue, a RAT (Register Aliasing Table), physical register files for both integer and floating point registers. It implements two ROBs (Re-Order Buffer), a speculative ROB which resides at the renaming stage and another ROB which resides at the commit stage and is used to recover from branch mispredictions and interrupts. 

It models a 6-stage pipeline (instruction fetch; decode; rename; issue; execute; write-back; commit). The source operands are read in the same stage as it is issued. All the stages takes exactly one cycle except for execute stage which can take several cycles depends on the instructions. We modified the relevant parts of the simulator in order to properly simulate the register file cache architecture. Table \ref{table:parameters} lists the major microarchitecture features we used throughout all the experiments. We implemented the register file cache (uppermost level) as a fully-associative organization with an LRU replacement policy. As of the caching policy and fetching mechanism we chose \textit{non-bypass} caching and \textit{prefetch-first-pair} fetching because this combination achieved the highest IPC in \cite{antonio00}.

\begin{table}[bhtp]
\tbl{Microarchitecture parameters used for simulation}{%
\begin{tabular}{|c|p{7cm}|}
\hline 
\textbf{Parameter} & \textbf{Value} \\ 
\hline \hline
$\mu$ops operands & 1 destination operand and up to 3 source operands (source operand can be either register or immediate value) \\
\hline
Fetch width & 4 instructions \\ 
\hline 
I-cache & 128 KB, 8 way-associative, 64 byte lines, 2-cycle hit time, 5-cycle miss time \\ 
\hline 
Branch predictor & Combined (Gshare with 2-bit bi-modal) \\ 
\hline 
Reorder buffer size & 128 \\
\hline
Issue queue size & 64 \\ 
\hline 
Functional units & 4 int, 4 FP, 4 load\slash store \\ 
\hline 
Load\slash store queue & 64 \\ 
\hline 
Issue mechanism & 4-way out-of-order issue \\ 
\hline 
Physical registers & 256 int \slash 256 FP \\ 
\hline 
D-cache & 128 KB, 8 way-associative, 64 byte lines, 2-cycle hit time, 5-cycle miss time \\ 
\hline 
Commit width & 4 instructions \\ 
\hline 
Register file access time & 2 cycles \\
\hline
Register file cache access time & 1 cycle \\
\hline
\end{tabular}}
%\caption{Microarchitecture parameters used for simulation}
\label{table:parameters}
\end{table}

For the evaluation of the register file cache architecture, we ran the SPEC CPU 2006 benchmarks \cite{spec_website} on the MARSSx86 simulator. SPEC CPU 2006 is the latest version of a series of benchmarks designed to provide a comparative measure of compute-intensive performance across the widest practical range of hardware using workloads developed from real user applications. It contains two benchmark suites: INT which is composed of 12 benchmarks and measures the compute-intensive integer performance; FP which is composed of 17 benchmarks and measures the compute-intensive floating point performance \cite{spec_art}. All the results presented in this paper are based upon the IPC (instruction per cycle) and other relevant hardware counter data from the execution of the benchmarks. 

Since the SPEC CPU 2006 benchmarks have significantly larger dynamic instruction counts and data footprint than the earlier SPEC CPU 2000 benchmarks \cite{spec_profile}, a full execution of the benchmarks on the simulator would take days even weeks to complete. In order to reduce the execution time while retain the accuracy of the simulation result we used some profiling and instrumentation techniques. We used Intel\textregistered PinPoints \cite{pinpoints} to analyse and dynamically instrument the benchmarks. Consequently, it generates a BBV (basic block vector) file of the instrumented benchmark which we then fed it to SimPoint \cite{simpoint}. Given the instruction slice (number of instructions) SimPoint will in turn pick up the most representative instruction slices and provide the weight of each of them. In this paper the instruction slice we used is 250 million which is reported to be relatively accurate \cite{spec_profile}. 

\subsection{Impact of process variations}
The constant presence of process variations renders the register file itself unstable. Cells of the register file could be temporarily malfunctioning or even broken over time thus reduces the register file size temporarily or permanently. Figure \ref{rf_rfc} illustrates the impact of the degradation of the register file on the performance. Here we assume a weak-strong pair architecture, the register file size degrades whereas the register file cache retains its original size. Also we introduce two baselines, a best-case scenario (an ideal 1-cycle monolithic register file) and a worst-case scenario (a 2-cycle monolithic register file with only one level of bypass network). We used four different sizes of the register file cache, we will devote the next section analysing their impacts on the performance.

\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.70]{rf_rfc_int.pdf}}
\centerline{\includegraphics[scale=0.70]{rf_rfc_fp.pdf}}
\caption{We assume a strong register file cache with a weak register file}
\label{rf_rfc}
\end{figure}

Not surprisingly, we observe a performance drop whenever the register file size decreases in both SPEC CPU 06 INT and FP benchmark suites. The performance degradation is within 5\% after the register file size decreases to 96 entries in the INT benchmark suite. For FP the degradation is within 9\%. As the register file size continues to decrease the performance curves become steeper because the simulator incorporates 80 physical registers, as the size of the register file is approaching down to or even below this number the register file would be getting difficult to serve all the requests from the issue queue on time. 

We also observe that despite some minor fluctuations, among the four sizes of the register file cache the performance curves see a rather similar pattern. In the INT benchmark suite the register file cache architecture sees a greater degradation (2\% - 3\%) of performance than an ideal 1-cycle monolithic register file when the register file size is somewhere around 64 to 96 entries. However, the performance curves converge when the register file size keeps increasing or decreasing. Whereas in FP benchmark we do not see this pattern, all the curves follow a quite close if not identical pattern. 

\subsection{Impact of the register file cache size}
Similar to a conventional cache memory system the size of a register file cache is critical to achieving high performance. Figure \ref{cache_size} shows the performances of various register file cache sizes (8, 16, 24, 32 entries) compare to an ideal 1-cycle monolithic register file. We introduce another parameter here: the buses between the register file and the register file cache which we will discuss in the next section. Regardless of the number of buses in-between these two register file banks, there is a performance gain as the register file cache size increases. However, as we can observe in the Figure \ref{cache_size} the trend flattens when the register file cache size approaches 32 entries which implies a performance bottleneck of the register file cache architecture. 

Overall the register file cache architecture incurs performance lose of around 3\% - 10\% in SPEC CPU 2006 INT benchmark suite depending on the register file cache size compare to an ideal 1-cycle register file as well as the buses in-between. Nevertheless, it still possesses a 3\% - 10\% speed-up over a 2-cycle register file with one bypass level in both benchmark suites.

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{2bus_int.pdf}
\includegraphics[scale=0.50]{2bus_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{4bus_int.pdf}
\includegraphics[scale=0.50]{4bus_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{8bus_int.pdf}
\includegraphics[scale=0.50]{8bus_fp.pdf}
}
\caption{Performance of various register file cache sizes}
\label{cache_size}
\end{figure}
\newpage
\subsection{Usage of the buses}
In the previous section, we have seen that regardless the number of available buses between the register file and the register file cache the performance trends are nearly identical. This leads us to a further check. The results show in Figure \ref{bus0} and \ref{bus}. Obviously the performance of deploying 4 and 8 buses are nearly identical regardless the register file cache size while with 2 buses the performance sees a minor drop with the register file cache size of 8 and 16. However, a check on the actual statistic reveals that the performance drop is not significant, less than 2\%. Hence, of all the register file cache size we have experimented the impact of using either 2, 4 or 8 buses is not apparent which implies the rare usage of the buses.

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{8entries_int.pdf}
\includegraphics[scale=0.50]{8entries_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{16entries_int.pdf}
\includegraphics[scale=0.50]{16entries_fp.pdf}
}
\caption{Impact on performance of the number of buses between the RF and the RF Cache}
\label{bus0}
\end{figure}

\begin{figure}[hbtp]
\centerline{
\includegraphics[scale=0.50]{24entries_int.pdf}
\includegraphics[scale=0.50]{24entries_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{32entries_int.pdf}
\includegraphics[scale=0.50]{32entries_fp.pdf}
}
\caption{The buses in-between have minor impact on performance (continue.)}
\label{bus}
\end{figure}
\newpage
We gathered the bus usage information in every cycle during the simulation (shown in Figure \ref{ports_usage}). Due to the presence of the prefetching mechanism on top of the fetch-on-demand fetching policy, 2 buses are barely enough to meet the read requests to the register file cache. It is especially prominent for small register file caches. We can see in Figure \ref{ports_usage} that with a register file cache size of 8 and 2 buses these 2 buses are quite busy for more than 60\% of the cycles in the SPEC CPU 2006 INT benchmark suite. Naturally, the bus demands reduces with the increasing of the register file cache size since it is able to hold more registers at a time. When 4 or 8 buses are incorporated the cycles where there is no traffic becoming more and more prominent up to close to 50\% for the SPEC CPU 2006 INT benchmark suite and around 60\% for the SPEC CPU 2006 FP benchmark suite. This somehow explains why in these 2 bus configurations the number of buses does not matter any more. 

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{ports_usage_2_int.pdf}
\includegraphics[scale=0.50]{ports_usage_2_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{ports_usage_4_int.pdf}
\includegraphics[scale=0.50]{ports_usage_4_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{ports_usage_8_int.pdf}
\includegraphics[scale=0.50]{ports_usage_8_fp.pdf}
}
\caption{Bus usage with various bus numbers}
\label{ports_usage}
\end{figure}

\newpage

\subsection{Usage of the bypass network}
As we have seen in the last section, the bus traffic between the register file and the register file cache does not seem to be heavy, two buses is sufficient to achieve a relatively high performance with the maximum performance drop of less than 2\%. This could imply a frequent use of the bypass network. The percentage of use of the bypass network under various number of buses is shown in Figure \ref{bypass_usage}. First, we can observe that the number of buses has minor effect on the percentage of the usage of the bypass network. In SPEC06 INT benchmark suite of all the execution cycles around 50\% of which the bypass network is not used. This number decrements when the register file cache size becomes larger. On the other hand, a one-bypass usage takes place in around 34\% of all the execution cycles, it slightly increments corresponding to the increase of the register file cache size. The two-bypass and three-bypass usage, which occur less frequently, are 12\% and 2\% respectively. 

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{bypass_2bus_int.pdf}
\includegraphics[scale=0.50]{bypass_2bus_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{bypass_4bus_int.pdf}
\includegraphics[scale=0.50]{bypass_4bus_fp.pdf}
}
\centerline{
\includegraphics[scale=0.50]{bypass_8bus_int.pdf}
\includegraphics[scale=0.50]{bypass_8bus_fp.pdf}
}
\caption{Bypass usage with various bus numbers}
\label{bypass_usage}
\end{figure}

Given the frequent bypass network traffic, we set out to study its relationship with the $\mu$ops being executed. We distinguish the $\mu$ops by the number of source operands they have since the number of the source operands has direct impact on the bypass traffic. As in Table \ref{table:parameters}, each $\mu$ops in can have up to 3 source operands, we thus divide the $\mu$ops into 4 categories. Figure \ref{uops_usage} shows the harmonic mean of the percentage of the $\mu$ops of each category from both the SPEC06 INT and FP benchmark suites. We can observe that the $\mu$ops with 2 source operands are dominant (69\% in SPEC06 INT and 67.5\% in SPEC06 FP) compare to others. In Table \ref{table:uops} we list some typical $\mu$ops from each type of the category in hope for a better understanding of Figure \ref{uops_usage}:

From Table \ref{table:uops}, we can see that most of the commonly used $\mu$ops has 2 source operands which explains the dominantly high percentage of 2-source-operand $\mu$ops from Figure \ref{uops_usage}. 

\begin{table}[bhtp]
\tbl{Typical $\mu$ops of each category}{%
\begin{tabular}{|c|p{7cm}|}
\hline 
\textbf{Category} & \textbf{Typical $\mu$ops} \\ 
\hline \hline
$\mu$ops with no source operand & unconditional branch (\emph{bru}), \emph{nop}\\
\hline
$\mu$ops with 1 source operand & conditional branch (\emph{br.cc}), indirect jump (\emph{jmp}, \emph{jmpp}), FP convertion (\emph{cvtf})\\
\hline 
$\mu$ops with 2 source operands & \emph{mov}, logical (\emph{and}, \emph{xor} etc.), integer/FP arithmetic (\emph{add}, \emph{mul}, \emph{addf}, \emph{mulf} etc.), load (\emph{ld} etc.), shift (\emph{shl}, \emph{shr}), rotate (\emph{rotl}, \emph{rotr}), \emph{mask}, bit testing (\emph{bt}, \emph{btr}, etc.), \emph{cmpf}\\
\hline 
$\mu$ops with 3 source operands & store (\emph{st} etc.), conditional set/select (\emph{set.cc}, \emph{sel.cc}),  conditional compare and set (\emph{set.sub.cc}, \emph{set.and.cc}), \emph{collcc}\\
\hline 
\end{tabular}}
\label{table:uops}
\end{table}

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{uops_int.pdf}
\includegraphics[scale=0.50]{uops_fp.pdf}
}
\caption{Distribution of $\mu$ops with different numbers of source operands}
\label{uops_usage}
\end{figure}

We also studied how do 2-source-operand $\mu$ops get their source operand values (either from the register file cache or from the bypass network) and the result is shown in Figure \ref{uops_2operands}. As indicated in Figure \ref{bus} the buses has minor effect on the performance plus the statistic of different buses configurations does not differ much, we hence use the smallest number of buses possible (2 buses) since the buses can significantly increase the overall area and power. In the SPEC06 INT benchmark suite, the situation that the $\mu$ops read both their source operand values from the register file is around 45\% and this figure decrements as the register file cache size increases. In the SPEC06 FP the percentage is 6\% higher. This is not particularly surprising, given that the majority of the $\mu$ops has 2 source operands, the result in Figure \ref{uops_2operands} should reflect the overall trends shown in Figure \ref{bypass_usage}.

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{uops_2operands_int_2bus.pdf}
\includegraphics[scale=0.50]{uops_2operands_fp_2bus.pdf}
}
\caption{The distribution of sources where 2-source-operand $\mu$ops get their source operand values}
\label{uops_2operands}
\end{figure}

\newpage

We also would like to know the overall performance of the issue queue under the influence of the register file cache architecture by finding out the percentage of $\mu$ops failed being issued due to the absence of their operands values in the register file cache. However, there are several factors that can prevent a given $\mu$op from being issued (operand not ready, no available function units, already reach the issue width etc.). In order to rule out the rest of the factors, we modified the simulator to gather the following information: the number of $\mu$ops which are within the issue width and meet the \emph{fetch-on-demand} policy but do not have all the values of their operands available in the register file cache. The result is presented in Figure \ref{iq_wait}. We can see that in the SPEC06 INT benchmark suite for a register file cache size of 8 entries, the percentage is around 26\% and this number decreases when the capacity of the register file cache increments. Since the register file cache is able to hold more and more values, this becomes obvious. The trend of this flattens which indicates a possible bottleneck of the register file cache size. Similar percentage and tendency can also be observed in the SPEC06 FP benchmark suite. 

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{absence_int.pdf}
\includegraphics[scale=0.50]{absence_fp.pdf}
}
\caption{The percentage of $\mu$ops that could not be issued due to the absence of their operands values in the register file cache}
\label{iq_wait}
\end{figure}

\section{Power and Area Evaluation}
\label{sec:power_area}
Apart from studying the performance impact of the register file cache, we evaluated its impact on the power consumption and the area overhead as well. We chose the McPAT \cite{mcpat} from HP Labs as the evaluation framework. McPAT is an integrated power, area and timing modelling framework for multi-threaded, multi-core/many-core processors. It provides a set of complete hierarchical models from architecture to the technology level by the means of an XML-based interface.

Since the process variation has become and is predicted to be one of the greatest threaten to integrated circuits, we chose the model of Intel Xeon processor of 22 nm technology during the evaluation which is the most recent technology node available in the current version of McPAT. Marssx86 has support to McPAT by the means of properly dumping the simulation results to McPAT's XML interface. The only thing we modified is the register file read and writing statistics. The lowest-level register file is read when the data from it is brought to the register file cache whereas every destination operand value is written back to it in the writeback stage. On the other hand, every read request of the operations from the issue queue is a read operation to the register file cache and the write operation to it is a combination of both the data transfer between the register file and the register file cache and the destination operand values in the writeback stage that are not read by the bypass network. 

Figure \ref{area} is a comparison of both the overall processor area (mm$^2$) and the area of the register file among 4 different register file cache sizes and an ideal 1-cycle register file as well as a 2-cycle 1-bypass register file. Since we are adding up components on top of the original processor it is reasonable to expect an overhead in terms of the area and the larger the register file cache size is the larger the area overhead is. We assume that to make the register file more reliable the transistor that the 2-cycle 1-bypass register file made of would be slightly larger. We suppose it would take 30\% more space than that of an ideal 1-cycle register file. We can see from Figure \ref{area} that the register file caches still take less area than a 2-cycle 1-bypass register file. 

A register file cache architecture implies more transistors over a conventional register file and it  inevitably increases the power consumption. Our result shows that the register file cache architecture incurs an overhead of 0.4\% and generally the overhead is positively correlated to the register file cache size.

\begin{figure}[bhtp]
\centerline{
\includegraphics[scale=0.50]{area_overall.pdf}
\includegraphics[scale=0.50]{area_rf.pdf}
}
\caption{Area of the processor and register file with/without register file cache}
\label{area}
\end{figure}

%\begin{figure}[bhtp]
%\centerline{
%\includegraphics[scale=0.50]{power_overall.pdf}
%\includegraphics[scale=0.50]{power_rf.pdf}
%}
%\caption{Power consumption of the processor and register file w/o register file cache}
%\label{power}
%\end{figure}

We further made an energy delay square metric diagram shown in which the numbers are relative to the result from the 2-cycle 1-bypass register file (Figure \ref{energy_delay}). It shows that in terms of energy efficiency the register file cache size of 8, 16, 24, 32 possess a rather similar efficiency (around 72\% - 79\% compare to the 2-cycle 1-bypass register file). Without any doubt the ideal 1-cycle register file achieves is 33\% more energy-efficient than a 2-cycle 1-bypass register file. 

\begin{figure}[bhtp]
\centerline{\includegraphics[scale=0.60]{energy_delay.pdf}}
\caption{The Energy-Delay Square metrics}
\label{energy_delay}
\end{figure}

\newpage

\section{Related Works}
\label{sec:related_works}
The recent researches on the register file has their focuses on the energy/thermal efficiency for both general purpose processors and embedded processors as well as techniques against the process variation. In \cite{customization} and \cite{rfc} the authors both proposed to use a heterogeneous multiple register file bank architecture to take advantage of the asymmetric use of the registers in common application to reduce the power consumption and eliminate heat density on this region. The authors in \cite{partitioning} proposed to partition the register file into hot and cold regions to reduce the overall power consumption. \\
On the other hand, people started to realize the significant impact from various forms of  process variations upon SRAM array including the register file. Apart from various circuit-level techniques such as adaptive body biasing and source biasing mentioned in \cite{survey}, there also exists system-level solutions. In \cite{mitigate} the authors suggested the use of variable-latency techniques to mitigate the impact of variations on the register file and execution units in a microprocessor. Also in \cite{nbti} and \cite{soft} the authors addressed some specific type of process variations: NBTI (negative biased temperature instability) and soft error respectively. 

\newpage

\section{Conclusion}
\label{sec:conclusion}
In this paper, we propose to use a cache-like organization of the register file in order to mitigate the performance drop caused by the process variation under current and future technologies or excessive use of the guard-banding. Since it is an architecture that has already been proposed before, we focus on using simulations to measure its performance and the influence of some of its key characteristics (register file cache size, buses in between the register file and the register file cache etc.). 

We first observe that for a weak register file (which means that the size of the register file itself is shrinking over time) the register file cache architecture is able to retain most of its performance until the point in which the size of the register file is close to or even smaller than the physical registers of a given processor. 

We then set out to find out some key features of this architecture and their relationships with the performance. The result shows that the size of the register file cache helps to prevent some of the performance loses to some point (in our case the register file cache size of 32). Meanwhile, the number of buses in-between the register file and the register file cache plays a minor role as far as performance is concerned. We go further to dig up the reason of this. Several experiments on bypass usage indicate that the bypass network on the process has been extensively used. We study the 2-operand $\mu$ops, which is the type of majority of the $\mu$ops, it turns out that these $\mu$ops heavily use the bypass network which renders the buses in-between the register file and the register file cache idle in many cycles. 

As an additional component on the processor the power consumption and the extra area are also a big concern. A register file cache architecture implies more transistors over a conventional register file and it inevitably increases the power consumption. Our study shows that the area increasing is around 0.2\% and is positively correlated with the register file cache size. On the other hand, the overall power consumption overhead is around 0.4\%. Nevertheless, the positive correlation as seen in the area comparison breaks at the register file cache size of 32, it actually consumes less power than the register file cache size of 24. 

Under the process variation the register file cache architecture is shown to be a better choice than a 2-cycle register file using guard-banding bigger transistors in terms of performance. This architecture enables the use of some other techniques to further tackle the process variation problems. This is out of the scope of this paper but it could open new doors to the research on process variation.

\newpage
\bibliographystyle{ACM-Reference-Format-Journals}
\bibliography{rfc_taco_bib}
\end{document}

