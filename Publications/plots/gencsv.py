#!/usr/bin/env python3

BUSES=("2-bus", "4-bus", "8-bus")
RFC_SIZE=("8entries", "16entries", "24entries", "32entries")
USAGE=("0bypass", "1bypass", "2bypasses", "3bypasses")
USAGE0=("0bus", "1bus", "2bus", "3bus", "4bus", "5bus", "6bus", "7bus", "8bus")
SPEC=("int", "fp")
RP="0.    "

print ("\"\",\"Buses\",\"RFC_Size\",\"RP\",\"usage\",\"SPEC\"")

def gen_bypass():
    i=1
    for spec in SPEC:
        for bus in BUSES:
            for size in RFC_SIZE:
                for usage in USAGE:
                    print('"%s","%s","%s",0.      ,"%s","%s"' % (i, bus, size, usage, spec))
                    i+=1

def gen_bus():
    i=1
    for spec in SPEC:
        for bus in BUSES:
            for size in RFC_SIZE:
                if bus == "2-bus":
                    usage = 3
                elif bus == "4-bus":
                    usage = 5
                elif bus == "8-bus":
                    usage = 9
                for b in range(usage):
                    print('"%s","%s","%s",0.      ,"%s","%s"' % (i, bus, size, USAGE0[b], spec))
                    i+=1


if __name__ == "__main__":
#    gen_bypass();
    gen_bus();
